//
//  CartasYuGiOh.swift
//  ExamenPrimerBimestre
//
//  Created by Jaime Yerovi on 13/12/17.
//  Copyright © 2017 Jaime Yerovi. All rights reserved.
//

import Foundation
import ObjectMapper

class Cards: Mappable {
    
    var type:String?
    var attack:Int?
    var deffense:Int?
    var stars:Int?
    

    required init(map: Map) {
    
    }
    
    func mapping(map: Map) {
        type <- map["type"]
        attack <- map["attack"]
        deffense <- map["deffense"]
        stars <- map["stars"]
        
    }
}



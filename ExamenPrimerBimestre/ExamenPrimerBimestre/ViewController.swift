//
//  ViewController.swift
//  ExamenPrimerBimestre
//
//  Created by Jaime Yerovi on 13/12/17.
//  Copyright © 2017 Jaime Yerovi. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController {

    //MARK:- Outlets
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var attackLabel: UILabel!
    
    
    @IBOutlet weak var deffenseLabel: UILabel!
    
    @IBOutlet weak var starsLabel: UILabel!
    
    @IBOutlet weak var searchTextField: UITextField!
    
    
    //MARK:- View
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    //MARK:- Actions
    
    @IBAction func consultarButtonPressed(_ sender: Any) {
        
        let nameCard = searchTextField.text ?? ""
        Alamofire.request("https://www.ygohub.com/api/card_info?name=\(nameCard)").responseObject { (response: DataResponse<Cards>) in
            
            let cards = response.result.value
            
            
            
            DispatchQueue.main.async {
                self.nameLabel.text = cards?.type ?? ""
                self.attackLabel.text = "\(cards?.attack ?? 0)"
                self.deffenseLabel.text = "\(cards?.deffense ?? 0)"
                self.starsLabel.text = "\(cards?.stars ?? 0)"
            }
            
            
        }
        
        
    }
    

}

